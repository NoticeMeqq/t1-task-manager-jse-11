package ru.t1.annenkovsv.tm.service;

import ru.t1.annenkovsv.tm.api.repository.ITaskRepository;
import ru.t1.annenkovsv.tm.api.service.ITaskService;
import ru.t1.annenkovsv.tm.model.Task;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        taskRepository.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
