package ru.t1.annenkovsv.tm.component;

import ru.t1.annenkovsv.tm.api.controller.ICommandController;
import ru.t1.annenkovsv.tm.api.controller.IProjectController;
import ru.t1.annenkovsv.tm.api.controller.ITaskController;
import ru.t1.annenkovsv.tm.api.repository.ICommandRepository;
import ru.t1.annenkovsv.tm.api.repository.IProjectRepository;
import ru.t1.annenkovsv.tm.api.repository.ITaskRepository;
import ru.t1.annenkovsv.tm.api.service.ICommandService;
import ru.t1.annenkovsv.tm.api.service.IProjectService;
import ru.t1.annenkovsv.tm.api.service.ITaskService;
import ru.t1.annenkovsv.tm.constant.ArgumentConst;
import ru.t1.annenkovsv.tm.constant.CommandConst;
import ru.t1.annenkovsv.tm.controller.CommandController;
import ru.t1.annenkovsv.tm.controller.ProjectController;
import ru.t1.annenkovsv.tm.controller.TaskController;
import ru.t1.annenkovsv.tm.repository.CommandRepository;
import ru.t1.annenkovsv.tm.repository.ProjectRepository;
import ru.t1.annenkovsv.tm.repository.TaskRepository;
import ru.t1.annenkovsv.tm.service.CommandService;
import ru.t1.annenkovsv.tm.service.ProjectService;
import ru.t1.annenkovsv.tm.service.TaskService;
import ru.t1.annenkovsv.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);


    public void run(String[] args) {
        processArguments(args);
        processCommands();
    }

    private void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("** ENTER COMMAND **");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    private void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument.toUpperCase()) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showMemoryInfo();
                break;
            default:
                commandController.showErrorArgument();
                break;
        }
        System.exit(0);
    }

    private void processCommand(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument.toUpperCase()) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showMemoryInfo();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case CommandConst.EXIT:
                commandController.showExit();
            default:
                commandController.showErrorCommand();
                break;
        }
    }

}
