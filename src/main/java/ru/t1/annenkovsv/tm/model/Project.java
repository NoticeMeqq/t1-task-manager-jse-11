package ru.t1.annenkovsv.tm.model;

import java.util.UUID;

public class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (id != null && !id.isEmpty())
            result += "Id: " + id;
        if (name != null && !name.isEmpty())
            result += ", Name: " + name;
        if (description != null && !description.isEmpty())
            result += ", Description: " + description;
        return result;
    }

}