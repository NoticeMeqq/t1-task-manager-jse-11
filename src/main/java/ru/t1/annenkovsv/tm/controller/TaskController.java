package ru.t1.annenkovsv.tm.controller;

import ru.t1.annenkovsv.tm.api.controller.ITaskController;
import ru.t1.annenkovsv.tm.api.service.ITaskService;
import ru.t1.annenkovsv.tm.model.Task;
import ru.t1.annenkovsv.tm.util.TerminalUtil;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATING TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[SUCCESS]");
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOWING TASK]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
        }
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEARING TASK]");
        taskService.clear();
        System.out.println("[SUCCESS]");
    }

}
