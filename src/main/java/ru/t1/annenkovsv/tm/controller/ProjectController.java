package ru.t1.annenkovsv.tm.controller;

import ru.t1.annenkovsv.tm.api.controller.IProjectController;
import ru.t1.annenkovsv.tm.api.service.IProjectService;
import ru.t1.annenkovsv.tm.model.Project;
import ru.t1.annenkovsv.tm.util.TerminalUtil;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATING PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        else System.out.println("[SUCCESS]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEARING PROJECTS]");
        projectService.clear();
        System.out.println("[SUCCESS]");
    }

    @Override
    public void clearProjectByIndex() {
        System.out.println("[CLEARING PROJECTS]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("[SUCCESS]");
    }

    @Override
    public void clearProjectById() {
        System.out.println("[CLEARING PROJECTS]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("[SUCCESS]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() -1;
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        else System.out.println("[SUCCESS]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        else System.out.println("[SUCCESS]");
    }

    @Override
    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOWING PROJECTS]");
        int index = 1;
        final List<Project> projects = projectService.findAll();
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOWING PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        showProject(project);
        System.out.println("[SUCCESS]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOWING PROJECT BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        showProject(project);
        System.out.println("[SUCCESS]");
    }

}
