package ru.t1.annenkovsv.tm.repository;

import ru.t1.annenkovsv.tm.api.repository.IProjectRepository;
import ru.t1.annenkovsv.tm.model.Project;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public Project add(final Project project){
        projects.add(project);
        return project;
    }

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        add(project);
        return project;
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project findByIndex(final Integer index) {
       return projects.get(index);
    }

    @Override
    public Project findById(final String id) {
        for (final Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project remove(final Project project) {
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        return remove(project);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        return remove(project);
    }

}
