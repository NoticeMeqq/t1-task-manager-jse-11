package ru.t1.annenkovsv.tm.repository;

import ru.t1.annenkovsv.tm.api.repository.ITaskRepository;
import ru.t1.annenkovsv.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> task = new ArrayList<>();

    @Override
    public void add(final Task project){
        task.add(project);
    }

    @Override
    public List<Task> findAll() {
        return task;
    }

    @Override
    public void clear() {
        task.clear();
    }

}
