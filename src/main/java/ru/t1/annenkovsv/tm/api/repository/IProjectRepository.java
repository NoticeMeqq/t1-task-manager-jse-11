package ru.t1.annenkovsv.tm.api.repository;

import ru.t1.annenkovsv.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    Project findByIndex(Integer index);

    Project findById(String id);

    void clear();

    Project remove(Project project);

    Project removeByIndex(Integer index);

    Project removeById(String id);
}
