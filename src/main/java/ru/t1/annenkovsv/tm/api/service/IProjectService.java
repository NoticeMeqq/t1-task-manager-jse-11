package ru.t1.annenkovsv.tm.api.service;

import ru.t1.annenkovsv.tm.model.Project;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    Project findByIndex(Integer index);

    Project findById(String id);

    void clear();

    Project removeByIndex(Integer index);

    Project removeById(String id);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);
}
