package ru.t1.annenkovsv.tm.api.controller;

import ru.t1.annenkovsv.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void clearProjectByIndex();

    void clearProjectById();

    void updateProjectById();

    void updateProjectByIndex();

    void showProjects();

    void showProject(Project project);

    void showProjectByIndex();

    void showProjectById();
}
