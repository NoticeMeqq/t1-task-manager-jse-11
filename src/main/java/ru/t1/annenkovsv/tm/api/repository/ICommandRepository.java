package ru.t1.annenkovsv.tm.api.repository;

import ru.t1.annenkovsv.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

    String getArgumentsValue();

    String getCommandsValue();

}
