package ru.t1.annenkovsv.tm.constant;

public final class CommandConst {

    public static final String HELP = "HELP";

    public static final String VERSION = "VERSION";

    public static final String ABOUT = "ABOUT";

    public static final String INFO = "INFO";

    public static final String EXIT = "EXIT";

    public static final String PROJECT_LIST = "LISTPROJECTS";

    public static final String PROJECT_CLEAR = "CLEARPROJECTS";

    public static final String PROJECT_CREATE = "CREATEPROJECT";

    public static final String PROJECT_SHOW_BY_ID = "CREATEPROJECTBYID";

    public static final String PROJECT_SHOW_BY_INDEX = "CREATEPROJECTBYINDEX";

    public static final String PROJECT_UPDATE_BY_ID = "UPDATEPROJECTBYID";

    public static final String PROJECT_UPDATE_BY_INDEX = "UPDATEPROJECTBYINDEX";

    public static final String PROJECT_DELETE_BY_ID = "DELETEPROJECTBYID";

    public static final String PROJECT_DELETE_BY_INDEX = "DELETEPROJECTBYINDEX";

    public static final String TASK_LIST = "LISTTASKS";

    public static final String TASK_CLEAR = "CLEARTASKS";

    public static final String TASK_CREATE = "CREATETASK";

    public static final String TASK_SHOW_BY_ID = "SHOWTASKBYID";

    public static final String TASK_SHOW_BY_INDEX = "SHOWTASKBYINDEX";

    public static final String TASK_UPDATE_BY_ID = "CREATETASKBYID";

    public static final String TASK_UPDATE_BY_INDEX = "CREATETASKBYINDEX";

    public static final String TASK_DELETE_BY_ID = "DELETETASKBYID";

    public static final String TASK_DELETE_BY_INDEX = "DELETEPROJECTBYINDEX";

    private CommandConst() {
    }

}